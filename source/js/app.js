function fixHeader() {
  var scroll = $(window).scrollTop();

  if (scroll >= 10) {
    $("#heading-s").addClass("heading-scroll");
  } else {
    $("#heading-s").removeClass("heading-scroll");
  }
}

$(window).scroll(fixHeader);

$(document).ready(fixHeader);

function removeCarousel() {
  const widthScreen = window.innerWidth;
  if (widthScreen > 1000) {
    const dicas = document.getElementsByClassName("carousel-atuation");

    console.log(dicas);

    dicas[0].classList.remove("carousel-atuation");
  }
}

//masks

$(".telefone").inputmask({
  mask: ["(99)9999-9999", "(99)99999-9999"],
  keepStatic: true,
});

$(".names").inputmask({ regex: "[a-zA-Z]*" });

/*   removeCarousel(); */

function sliderAtuation() {
  tns({
    container: ".carousel-atuation",
    items: 1,
    autoHeight: true,
    responsive: {
      900: {
        items: 4,
      },
    },
    lazyload: true,
    controlsContainer: ".controls-atuation",
    slideBy: "page",
    navContainer: ".nav-atuation",
    autoplay: false,
    loop: false,
  });
}

function certificaCarousel() {
  var certificaCarousel = tns({
    container: ".certifica-carousel-desktop",
    items: 1,
    responsive: {
      640: {
        items: 3,
        gutter: 10,
      },
    },
    touch: true,
    mouseDrag: true,
    lazyload: true,
    controlsContainer: ".certifica-btn",
    slideBy: "page",
    navContainer: ".nav-certifica",
    nav: false,
    autoHeight: true,
    autoplay: false,
    loop: true,
  });
}

function sliderForm() {
  tns({
    container: ".my-carousel",
    items: 1,
    lazyload: true,
    controlsContainer: ".carBtn",
    slideBy: "page",
    nav: false,
    autoHeight: true,
    swipeAngle: false,
    autoplay: false,
    loop: false,
  });
}
