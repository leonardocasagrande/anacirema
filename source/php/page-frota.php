<!-- HEADER -->
<?php get_header() ?>


<!-- BANNER -->
<div class="banner banner-frota mb-5">
  <?php
  if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<p id="breadcrumbs" class="d-none d-lg-block">', '</p>');
  }
  ?>
  <h1 class="color-blue">Frota</h1>
  <p class="d-md-none d-block color-grey">A frota da Anacirema Transportes e Logística é composta de veículos e equipamentos novos que atendem plenamente as necessidades de clientes em vários segmentos da indústria e da prestação de serviços. Entre seus veículos estão:</p>
</div>

<p class="d-md-block d-none frota-text">A frota da Anacirema Transportes e Logística é composta de veículos e equipamentos novos que atendem plenamente as necessidades de clientes em vários segmentos da indústria e da prestação de serviços. <br> <br> Entre seus veículos estão:</p>

<div class="container">

  <div class="d-md-flex justify-content-center d-block">

    <div class=" col-md-6 ">


      <div class="frota-img-1"></div>
      <h1>Cavalos</h1>
      <div class="frota-detalhe"></div>
      <div class="text-left">
        <p>· Cavalos mecânicos 4x2 – simples;</p>
        <p>· Cavalos mecânicos 6x2 –<span>trucado;</span></p>
        <p>· VUC delivery carroceria tipo sider.</p>
      </div>

    </div>

    <div class=" col-md-6 my-5 my-lg-0">

      <div class="frota-img-2"></div>
      <h1>Diferenciais</h1>
      <div class="frota-detalhe"></div>
      <div class="d-lg-flex">
        <div class="text-left">
          <p>· Pisos em chapa ou roletados;</p>
          <p>· Portas PPW (tipo roll up);</p>
          <p>· Suspensão pneumática;</p>

        </div>
        <div class="text-left">
          <p>· Sistema Double Deck/56 paletes.</p>
        </div>
      </div>
    </div>
  </div>


  <div class="margin-md-frota justify-content-center d-md-flex align-items-center mt-4">

    <div class="  col-md-6">
      <div class="frota-img-1"></div>
      <h1>Equipamentos</h1>
      <div class="frota-detalhe"></div>

      <div class="d-lg-flex">
        <div class="text-left">
          <p>· Carretas tipo sider e baú/28 paletes;</p>
          <p>· Carretas tipo sider/30 paletes;</p>
          <p>· Carretas tipo sider para 35 toneladas;</p>

        </div>
        <div class="text-left">

          <p>· Bug 20’ e 40’;</p>
          <p>· Carretas carga seca para 30 toneladas.</p>
        </div>
      </div>
    </div>

    <div class="bg-blue md-frota-style col-md-6 align-self-center p-5 ">
      <p class="text-white">A garantia de segurança do seu transporte é feita por meio de um sistema de rastreamento 24 horas, de um completo gerenciamento de risco e de uma equipe treinada, assim sua empresa pode enviar ou receber suas cargas com total tranquilidade.</p>
    </div>
  </div>

</div>


<!-- SECTION SISTEMAS DESKTOP -->
<section class=" bg-yellow ">
  <div class="container">
    <div class="d-lg-flex d-none align-items-center  justify-content-between ">
      <div class="frota-img-4 "></div>
      <h2 class=" h1-md-frota color-blue">Sistemas</h2>
    </div>

    <div class=" position-div justify-content-center col-12 py-4">

      <div class="frota-info mr-5 d-md-flex flex-wrap carousel-atuation">

        <div class="col-md-6">
          <h3>► Sistema de
            comunicação
            móvel via
            satélite</h3>
          <p>Comunicação permanente com o responsável pelo transporte da carga.</p>
        </div>

        <div class="col-md-6">
          <h3> ► Sistema de rastreamento ativo</h3>
          <p>Especialmente desenvolvido para a prevenção de roubos de cargas, com o acompanhamento dos veículos através de sistema de transmissão de dados via satélite.</p>
        </div>

        <div class="col-md-6">
          <h3>► EDI / Electronic Data Interchange</h3>
          <p>Sistema que garante a integridade dos dados de transporte e agiliza os processos junto aos clientes.</p>
        </div>

        <div class="col-md-6">
          <h3>► Sistema ERP / Rodopar</h3>
          <p>Gestão Integrada de Processos.</p>
        </div>

        <div class="col-md-6">
          <h3>► TMS / Transportation Management System</h3>
          <p>Gerenciamento de Operações de Transporte. </p>
        </div>

        <div class="col-md-6">
          <h3>► CTe / sistema de conhecimento eletrônico</h3>
          <p>A Anacirema Transportes e Logística foi a primeira empresa de transporte de cargas a implantar o Sistema de Conhecimento Eletrônico, que armazena os dados da carga eletronicamente e documenta o seu transporte, permitindo, entre outras coisas, o rastreamento da carga ponto-a-ponto.</p>
        </div>



      </div>

      <div class="d-flex d-lg-none px-4 align-items-center justify-content-between z-indexx">


        <div class="nav-atuation dots">

          <button></button>
          <button></button>
          <button></button>
          <button></button>
          <button></button>
          <button></button>

        </div>

        <div class="controls-atuation d-flex controls-carousel z-indexx mt-n4">

          <div class="prev text-white"> &#60; </div>

          <div class="next ml-5 text-white"> &#62; </i>


          </div>

        </div>

      </div>
    </div>
</section>



<!--  SECTION SISTEMAS MOBILE -->
<!-- <section class="d-md-none">
  <div class="atuation">
    <div class="container text-center">
      <h2 class="color-blue">Sistemas</h2>
    </div>
    <div class="d-lg-flex flex-lg-wrap carousel-atuation ">

      <div class="col-lg-3 item">
        <div class="box">
          <h3 class="">TMS / Transportation Management System</h3>
          <p>Gerenciamento de Operações de Transporte. </p>
        </div>
      </div>



      <div class="col-lg-3 item ">
        <div class="box">
          <h3 class="">Sistema de rastreamento ativo</h3>
          <p>Especialmente desenvolvido para a prevenção de roubos de cargas, com o acompanhamento dos veículos através de sistema de transmissão de dados via satélite.</p>
        </div>
      </div>



      <div class="col-lg-3 item ">
        <div class="box">
          <h3 class="">Sistema ERP / Rodopar</h3>
          <p>Gestão Integrada de Processos.</p>
        </div>
      </div>

      <div class="col-lg-3 item ">
        <div class="box">
          <h3 class="">EDI / Electronic Data Interchange</h3>
          <p>Sistema que garante a integridade dos dados de transporte e agiliza os processos junto aos clientes.</p>
        </div>
      </div>

      <div class="col-lg-3 item ">
        <div class="box">
          <h3 class="">Sistema de
            comunicação
            móvel via
            satélite</h3>
          <p>Comunicação permanente com o responsável pelo transporte da carga.</p>
        </div>
      </div>


      <div class="col-lg-3 item">
        <div class="box">
          <h3 class="">CTe / sistema de conhecimento eletrônico</h3>
          <p>A Anacirema Transportes e Logística foi a primeira empresa de transporte de cargas a implantar o Sistema de Conhecimento Eletrônico, que armazena os dados da carga eletronicamente e documenta o seu transporte, permitindo, entre outras coisas, o rastreamento da carga ponto-a-ponto.</p>
        </div>
      </div>

    </div>

    <div class="d-flex d-lg-none px-5 align-items-center justify-content-between">
      <div class="nav-atuation dots">

        <button></button>
        <button></button>
        <button></button>
        <button></button>
        <button></button>
        <button></button>

      </div>
      <div class="prevBtn d-flex controls-atuation">
        <div class="prev"> &#60; </div>
        <div class="next ml-5"> &#62; </i>
        </div>
      </div>
    </div>
  </div>

</section>
 -->

<!-- BANNER CAMINHAO -->
<?php echo get_template_part('descubra-tudo'); ?>

<!-- FORM -->
<?php echo get_template_part('contact-form'); ?>



<!-- FOOTER -->

<?php get_footer() ?>