<section>

    <div class="request">

        <div class="container  pt-5 py-lg-0 h-100">

            <div class="d-flex pt-5  py-lg-0 align-items-center h-100">

                <div class="col-lg-4 d-none d-lg-block"></div>

                <div class="d-lg-flex col-lg-8 text-center text-lg-left align-items-center">

                    <div class="col-lg-6">

                        <h2 class="text-white">Descubra tudo
                            que podemos
                            fazer pra você!</h2>

                    </div>

                    <div class="col-lg-6">

                        <div class="mt-5 pt-5 pt-lg-0 mt-lg-0">

                            <a href="<?= get_home_url()?>/orcamento/" class="btn-yellow"> Solicitar orçamento </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>