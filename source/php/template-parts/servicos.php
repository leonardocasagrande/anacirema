<section>

    <div class="services bg-blue">

        <div class="container text-center">

            <h2 class="text-white">Serviços</h2>

            <div class="row text-center">

                <div class="col-lg-4 item">

                    <div class="img-item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/tracking.png" alt="Transporte Door to Door - Anacirema Transportes e Logística" title="Transporte Door to Door - Anacirema Transportes e Logística"> <br>

                    </div>

                    <h3 class="color-blue">Transporte <br>
                        Door to Door</h3>

                    <p>Contratação do serviço <br> para entrega ponto a ponto <br> em transporte dedicado.</p>

                </div>

                <div class="col-lg-4 item">

                    <div class="img-item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/fullness.png" alt="Abastecimento em plantas - Anacirema Transportes e Logística" title="Abastecimento em plantas - Anacirema Transportes e Logística"> <br>

                    </div>

                    <h3 class="color-blue">Abastecimento <br>
                        em plantas
                    </h3>

                    <p>Onde existe a <br> movimentação e transporte <br> de containers ou isotanks.</p>

                </div>


                <div class="col-lg-4 item">

                    <div class="img-item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quimic.png" alt="Transporte de
produtos químicos - Anacirema Transportes e Logística" title="Transporte de
produtos químicos - Anacirema Transportes e Logística"> <br>

                    </div>

                    <h3 class="color-blue">Transporte de <br>
                        produtos químicos
                    </h3>

                    <p>Atendendo a legislação e <br> todas as exigências dos <br> órgãos competentes.</p>

                </div>

                <div class="col-12">

                    <p class="mb-5 mt-3 pb-4 text text-lg-left">Todos os serviços da Anacirema Transportes e Logística contam com seguro e cobertura de carga contra danos e roubo, além de Seguro Ambiental 3.1.1, que presta assistência e dá cobertura ambiental de maneira integrada, com ênfase na prevenção e no gerenciamento do processo de atendimento a emergências.</p>

                    <div class=" mb-4">
                        <a class="btn-yellow" href="<?= get_home_url() ?>/orcamento/">Solicitar orçamento</a>
                    </div>

                </div>


            </div>



        </div>

    </div>

</section>