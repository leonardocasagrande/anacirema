  <!-- HEADER -->
  <?php get_header() ?>

  <!-- BANNER -->
  <div class="banner banner-contato mb-5">
    <?php
    if (function_exists('yoast_breadcrumb')) {
      yoast_breadcrumb('<p id="breadcrumbs" class="d-none d-lg-block">', '</p>');
    }
    ?>
    <h1 class="color-blue">Orçamento</h1>
  </div>


  <!-- TITULO -->
  <div class="d-md-none contato-form-header border-bottom ">
    <i class="fas fa-angle-left fa-2x pr-5"></i>
    <h2>Orçamento</h2>
    <i class="fas fa-angle-right fa-2x pl-5"></i>
  </div>

  <!-- FORM -->
  <div class="container d-md-block text-md-center  pt-md-5  pb-5  border-bottom ">

    <h5 class="font-weight-bold my-5 mt-lg-0"> Deixe seus dados que em breve nossa equipe entrará em contato com as informações do seu orçamento. </h5>

    <div class="d-md-none detail-title-contato mb-5"></div>

    <?php echo do_shortcode('[contact-form-7 id="18" title="Orçamento"]') ?>

  </div>


  <!-- FOOTER -->
  <?php get_footer() ?>