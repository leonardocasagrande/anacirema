<!-- HEADER -->
<?php get_header(); ?>


<!-- BANNER -->
<div class="banner banner-certifica">
  <?php
  if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<p id="breadcrumbs" class="d-none d-lg-block">', '</p>');
  }
  ?>
  <h1 class="color-blue">Certificações</h1>
  <p class="d-md-none color-grey d-block">A Anacirema Transportes e Logística possui as principais certificações do mercado, entre elas:</p>
</div>


<!-- CERTIFICAÇÕES -->

<p class="d-md-block d-none certifica-text container text-left px-0 py-5">A Anacirema Transportes e Logística possui as principais certificações do mercado, entre elas:</p>
<div class="bg-blue col-md-7 py-5  margin-center">

  <div class="d-block d-md-flex text-center text-md-left my-md-5 pl-md-5 justify-content-around ">
    <div class="mb-5 col-md-3 p-md-0 pb-3  ">
      <img style="height:137px; width: 140px" src="../wp-content/themes/anacirema/dist/img/isopng.png" alt="">
    </div>

    <div class="px-5 pb-5 p-md-0 text-white m-md-0 col-3  mb-3 mx-5">
      <div class="separator"></div>
      <h1>Vigilância<br> Sanitária</h1>
    </div>

    <div class="px-5 pb-5 mb-3 p-md-0 text-white col-3  m-md-0 mx-5">
      <div class="separator"></div>
      <h1>Polícia<br>
        Civil</h1>
    </div>
  </div>


  <div class="d-block d-md-flex justify-content-around  pl-md-5">
    <div class="px-5 pb-5 mb-3 p-md-0 m-md-0 col-3  text-white mx-5">
      <div class="separator"></div>
      <h1>Polícia <br>
        Federal</h1>
    </div>

    <div class="px-5 pb-5 mb-3 p-md-0 m-md-0 col-3  text-white mx-5">
      <div class="separator"></div>
      <h1>Certificado <br>
        Acidente Zero</h1>
    </div>

    <div class="px-5 pb-5 p-md-0 m-md-0 col-3  text-white mx-5">
      <div class="separator"></div>
      <h1>Exército </h1>
    </div>
  </div>
</div>



<!-- CAROUSEL CERTIFICADOS -->

<section class="position-relative">

  <div class="desk-detail d-md-block d-none bg-yellow"></div>
  <div class=" bg-yellow  py-5 ">

    <div class="container">

      <div class="d-lg-flex pt-md-5 align-items-center justify-content-md-end">



        <div class="col-md-4">

          <div class=" text-center text-lg-left">
            <h2 class="color-blue pb-md-4">Certificados</h2>

          </div>


        </div>

        <div class="col-md-8 ">

          <div class=" certifica-carousel-desktop">

            <div class=" item">
              <div class="box bg-white  d-flex justify-content-center">
                <div class="certifica-logo certifica-1"></div>
              </div>
            </div>

            <div class=" item">
              <div class="box bg-white  d-flex justify-content-center">
                <div class="certifica-logo certifica-2"></div>
              </div>
            </div>

            <div class=" item">
              <div class="box bg-white  d-flex justify-content-center">
                <div class="certifica-logo certifica-3"></div>
              </div>
            </div>

            <div class=" item">
              <div class="box bg-white  d-flex justify-content-center">
                <div class="certifica-logo certifica-4"></div>
              </div>
            </div>

          </div>

          <div class="d-flex pl-lg-5 px-4 pr-md-3 mt-4 align-items-center justify-content-between ">


            <div class="nav-certifica dots">

              <button></button>
              <button></button>

            </div>


            <div class="controls-carousel d-flex  certifica-btn">
              <div class="prev"> &#60; </div>
              <div class="next ml-5"> &#62; </i>
              </div>
            </div>

          </div>

        </div>

      </div>

    </div>

  </div>


  </div>

</section>



<!-- BANNER ORÇAMENTO -->
<?php echo get_template_part('descubra-tudo'); ?>

<!-- FORM -->
<?php echo get_template_part('contact-form'); ?>


<!-- FOOTER -->
<?php get_footer(); ?>