  <!-- HEADER -->
  <?php get_header() ?>

  <!-- BANNER -->
  <div class="banner banner-contato mb-5">
    <?php
    if (function_exists('yoast_breadcrumb')) {
      yoast_breadcrumb('<p id="breadcrumbs" class="d-none d-lg-block">', '</p>');
    }
    ?>
    <h1 class="color-blue">Contato</h1>
    <p class="d-md-none color-grey">Escolha a melhor forma de entrar em contato com a Anacirema Transportes e Logística:</p>
  </div>


  <!-- TITULO -->
  <div class="d-md-none contato-form-header border-bottom ">
    <i class="fas fa-angle-left fa-2x pr-5"></i>
    <h2>Contato</h2>
    <i class="fas fa-angle-right fa-2x pl-5"></i>
  </div>

  <!-- FORM -->
  <div class="container">
    <h3 class="d-md-block d-none color-blue text-center mb-4">Escolha a melhor forma de entrar em contato com a Anacirema</br> Transportes e Logística:</h3>
  </div>

  <div class="d-flex container d-md-block text-md-center align-items-center  pb-5  flex-column  border-bottom ">

    <div class="d-md-none detail-title-contato mb-5"></div>

    <?php echo do_shortcode('[contact-form-7 id="15" title="Contato"]') ?>

  </div>


  <!-- FOOTER -->
  <?php get_footer() ?>